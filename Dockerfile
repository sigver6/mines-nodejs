FROM node:6.3.0-slim

RUN apt-get update && apt-get install -y vim curl git
RUN mkdir /proj
WORKDIR /proj
RUN mkdir /proj/dest

RUN npm install -g gulp-cli

ADD package.json /proj/package.json
ADD gulpfile.js /proj/gulpfile.js

COPY ./org.vimrc /root/.vimrc
COPY ./gulp /proj/gulp
COPY ./public /proj/public
COPY ./src /proj/src

RUN npm install

import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from   './auth-guard.service';
import { AuthService } from './auth.service';

import { LoginComponent } from './login.component';
import { loginRoutes, authProviders } from './login.routing';
import { CanDeactivateGuard } from './can-deactivate-guard.service';

import { DashboardComponent } from './dashboard.component';

const appRoutes: Routes = [
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [AuthGuard]
  }
];

export const appRoutingProvider: any[] = [
   AuthGuard,
   AuthService
];

export const routing = RouterModule.forRoot(appRoutes);

export class ExecuteInfo {
  constructor(
    public execute: string,
    public token: string
  ){}
}

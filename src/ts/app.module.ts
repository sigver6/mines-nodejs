import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { routing, appRoutingProvider } from './app.routing';

import { HttpModule, JsonpModule, XHRBackend } from '@angular/http';

import { ItemsModule } from './items.module';

import { ItemListComponent } from './app.list.component';
import { RegionComponent } from './app.region.component';
import { LoginComponent } from './login.component';
import { DashboardComponent } from './dashboard.component';
import { HighlightDirective } from './highlight.directive';

import { CustomPipe } from './custom.pipe';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    JsonpModule,
    routing,
    ItemsModule
  ],
  declarations: [
    AppComponent,
    DashboardComponent,
    RegionComponent,
    LoginComponent,
    HighlightDirective,
    CustomPipe
  ],
  providers: [
    appRoutingProvider
  ],
  bootstrap: [ AppComponent ]
})

export class AppModule { 
}

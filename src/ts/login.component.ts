import { Component, OnInit }                from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { AuthService }              from './auth.service';

import { LoginInfo } from './login.info';
import { ExecuteInfo } from './execute.info';

@Component({
  selector: 'login-form',
  templateUrl: 'html/login.html'
})

export class LoginComponent implements OnInit {

  public data: any;
  executeInfo: ExecuteInfo;
  errorMessage: string = "";
  model = new LoginInfo('', '');

  // for debug
  submitted = false;
 
  //public list:string[];

  constructor(public authService: AuthService, public router: Router) {
  }

  // for debug.
  onSubmit() { this.submitted = true; }

  get diagnostic() { return JSON.stringify(this.model); }

  // Reset the form with a new hero AND restore 'pristine' class state
  // by toggling 'active' flag which causes the form
  // to be removed/re-added in a tick via NgIf
  // TODO: Workaround until NgForm has a reset method (#6822)
  active = true;

  ngOnInit(){
      // write code if browser has session, move dashboard page.
      // this.router.navigate(['dashboard']);
  }


  newLoginInfo() {
    this.active = false;
    setTimeout(() => this.active = true, 0);
  }

  showFormControls(form: any) {
    return form && form.controls['name'] && form.controls['name'].value;
  }

  login(){
    this.authService.login(this.model)
                    .subscribe( res => { this.data = res } , 
                    error => this.errorMessage = <any>error ,
                    () => {this.afterlogin()});
  }

  private afterlogin(){
    if( this.errorMessage ==""){
      localStorage.setItem('auth_token',this.data.token);
      this.router.navigate(['dashboard']);
    } else {
      console.log('test error');
    }
  }

}

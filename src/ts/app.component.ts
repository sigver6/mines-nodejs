import { Component } from '@angular/core';

import './rxjs-operators';
import { Router, NavigationExtras } from '@angular/router';
import { AuthService } from './auth.service';
import { LoginComponent } from './login.component'


@Component({
  selector: 'my-app',
  templateUrl: 'html/app.component.html'
})

export class AppComponent{
  errorMessage: string;

  constructor(public authService: AuthService, public router: Router) {
  }


  logout(authService: AuthService){
    localStorage.removeItem('test');
    authService.logout();
    
    let navigationExtras: NavigationExtras = {
      preserveQueryParams: true,
      preserveFragment: true
    };

    this.router.navigate(['login']);
  } 
    
}

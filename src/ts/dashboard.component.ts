import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Item } from './item';
import { ItemService } from './item.service';

@Component({
  selector: 'my-dashboard',
  templateUrl: 'html/dashboard.component.html',
})

export class DashboardComponent implements OnInit {

  errorMessage: string;
  items: Item[];

  constructor(private router: Router, private itemService: ItemService) {
  }

  ngOnInit() {
    this.getItems();
  }

  getItems(){
    this.itemService.getItems()
                    .subscribe(
                      items => this.items = items,
                      error =>  this.errorMessage = <any>error);
  }
}

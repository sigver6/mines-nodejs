import { Component, OnInit } from '@angular/core';
import { Item } from './item';

import { ItemService } from './item.service';

@Component({
  selector: 'app-list',
  templateUrl: 'html/app.list.component.html',
})

export class ItemListComponent {
  errorMessage: string;
  items: Item[];


  constructor (private itemService: ItemService) {
  }


  ngOnInit() { this.getItems(); }

  getItems() {
    this.itemService.getItems()
                     .subscribe(
                       items => this.items = items,
                       error =>  this.errorMessage = <any>error);
  }
/*
  addItem (name: string) {
    if (!name) { return; }
    this.itemService.addItem(name)
                     .subscribe(
                       item  => this.itemes.push(item),
                       error => this.errorMessage = <any>error);
  }

  updateItem(item: Item){
    this.itemService.updateHelo(item.id)
                    .subscribe(               
                      item  => 
                      error => this.errorMessage = <any>error);    
  }

  deleteItem(id: integer){
    this.itemService.deleteHelo(id)
                    .subscribe(
                       item  => this.itemes.splice(id),
                       error => this.errorMessage = <any>error);
  }
*/

}


/*
Copyright 2016 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
